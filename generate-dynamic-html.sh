#!/bin/sh

SCRIPT="$0"
APP_DIR="$1"

PROJ_DIR=`pwd`
SPEC="college-registration-api.json"

if [ -z "${APP_DIR}" ]; then
  APP_DIR="../../../External/swagger-codegen"
  echo "Defaulting swagger-codegen directory to ${APP_DIR}"
fi

if [ ! -d "${APP_DIR}" ]; then
  echo "swagger-codegen directory does not exist or is not a directory" >&2
  exit 1
fi

executable="./modules/swagger-codegen-cli/target/swagger-codegen-cli.jar"

cd ${APP_DIR}

if [ ! -f "$executable" ]
then
  mvn package
fi

# if you've executed sbt assembly previously it will use that instead.
export JAVA_OPTS="${JAVA_OPTS} -XX:MaxPermSize=256M -Xmx1024M -DloggerPath=conf/log4j.properties"
args="generate -i ${PROJ_DIR}/${SPEC} -l dynamic-html -o ${PROJ_DIR}/html-dynamic"

java $JAVA_OPTS -jar $executable $args
