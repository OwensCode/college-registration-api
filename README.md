College Registration REST API
==============================

This is a REST API for a college registration system, created as a homework
assignment for the 2015 Architecture Boot Camp for HS2 Solutions.

The latest static html is included in this repository, but it's inferior to
the dynamic site available through the Swagger Editor (http://editor.swagger.io).

Scripts for generating the static html and a slightly more dynamic version that
is served up through Node.js are also included. This dynamic version is also
inferior to the content served by the Swagger Editor.

HS2 Solutions is a great place to work and we're always looking for quality
team members. Checkout us out at http://www.hs2solutions.com.